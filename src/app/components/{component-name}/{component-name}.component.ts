import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-name',
  templateUrl: './{component-name}.component.html',
  styleUrls: ['./{component-name}.component.css']
})
export class {ComponentName}Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
