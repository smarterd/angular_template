import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

const APP_ROUTES: Routes = [
    // { path: '', component: {IMPORTED COMPONENT} },
]

export const routing = RouterModule.forRoot(APP_ROUTES, { preloadingStrategy: PreloadAllModules });
