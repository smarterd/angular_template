jQuery(document).ready(function(){

  jQuery(this).scrollTop(0);
  // load

  jQuery(function() {
      //
      // $("#load img").delay(500).animate({"opacity": "1"}, 800, function() {
      //   $("#load").delay(500).animate({"opacity": "0"}, 900, function() {
      //   });
    // });

      $(this).scrollTop(0);
  });

  // Add Scrollspy

  // TODO: replace scrollspy with scrollIntoView with proper offset.

  $('body').scrollspy({target: '.navbar'});


  $('.navbarLink-s a').on('click', function(event) {

    if (this.hash !== "") {

      event.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){

        window.location.hash = hash;
      });
    }
  });

  // Load Animation

  // On Scroll



  // nav icons

  function navIconToggle() {
    $('.navicon-button').toggleClass('open');
    $('.navbarLink-collapse').toggleClass('navbarLink-collapse-open');
    if ( $('div').hasClass('navbarLink-collapse-open') ){
      $('#wrapper').not('.navbarLink-collapse-open').css('filter', 'blur(3px)')
    } else {
      $('#wrapper').not('.navbarLink-collapse-open').css('filter', 'none')
    };

  }

  $('.navicon-button').click(function(){
    navIconToggle();
  });

  $('.navbarLink-collapse').children().click(function(){
    navIconToggle();
  });

// Carousel click

$('#carouselButton').click(function() {
   $('.carousel').carousel(0);
});

});
